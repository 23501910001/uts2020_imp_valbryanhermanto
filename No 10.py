#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#Nomor 10
def fix_teen(n):
    if n >= 13 and n <= 19 and n != 15 and n != 16:
        return 0
    else:
        return n


def no_teen_sum(a,b,c):
    total = 0
    total += fix_teen(a)
    total += fix_teen(b)
    total += fix_teen(c)
    print(total)

no_teen_sum(1,2,3)
no_teen_sum(2,13,1)
no_teen_sum(2,1,14)

