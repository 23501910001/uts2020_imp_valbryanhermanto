#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#Nomor 8
def end_other(a, b):
    if a.lower() == b[len(a)::1].lower() or b.lower() == a[len(b)::1].lower():
        print('True')
    else:
        print('False')

end_other('abcKopi', 'abc')
end_other('AbC', 'HiaBc')
end_other('abc', 'abcXabc')
end_other('abc', 'abXabc')

